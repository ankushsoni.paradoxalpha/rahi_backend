const express = require("express");
const {
  GetAllGroups,
  GetAllGroupsOfSingleUser,
  GetAllMembersOfSingleGroup,
  UpdateGroup,
  DeleteGroup,
  GetSingleGroup,
  AddMembersInGroup,
  RemoveMemberFromGroup,
  CreateGroup,
  JoinGroupByGroupCode,
  UpdateGroupMember,
  GetAllMembersOfSingleGroup_2,
  ShareLocationWithGroup,
} = require("../../controler/group");
const { validate } = require("../../middleware/validate");
const {
  createGroupSchema,
  getAllMembersOfSingleGroupSchema,
  updateGroupSchema,
  deleteGroupSchema,
  getSingleGroupSchema,
  addMembersInGroupSchema,
  removeMemberFromGroupSchema,
  joinGroupByGroupCode,
  shareLocationSchema,
} = require("../../validation/group");
const { verifyToken } = require("../../middleware/auth");

const groupRoute = express.Router();

groupRoute.post(
  "/create",
  validate(createGroupSchema),
  verifyToken,
  CreateGroup
);
groupRoute.get("/get-all-groups", verifyToken, GetAllGroups);
groupRoute.get("/user/get-all-groups", verifyToken, GetAllGroupsOfSingleUser);
groupRoute.get(
  "/get-all-members",
  validate(getAllMembersOfSingleGroupSchema),
  verifyToken,
  GetAllMembersOfSingleGroup
);
groupRoute.get(
  "/get-all-members-2",
  validate(getAllMembersOfSingleGroupSchema),
  verifyToken,
  GetAllMembersOfSingleGroup_2
);
groupRoute.put(
  "/update",
  validate(updateGroupSchema),
  verifyToken,
  UpdateGroup
);
groupRoute.delete(
  "/delete",
  validate(deleteGroupSchema),
  verifyToken,
  DeleteGroup
);
groupRoute.get(
  "/get-single-group",
  validate(getSingleGroupSchema),
  verifyToken,
  GetSingleGroup
);
groupRoute.post(
  "/add-member-in-group",
  validate(addMembersInGroupSchema),
  verifyToken,
  AddMembersInGroup
);
groupRoute.delete(
  "/remove-member-from-group",
  validate(removeMemberFromGroupSchema),
  verifyToken,
  RemoveMemberFromGroup
);
groupRoute.post(
  "/join-group",
  validate(joinGroupByGroupCode),
  verifyToken,
  JoinGroupByGroupCode
);
groupRoute.patch("/update-group-member", verifyToken, UpdateGroupMember);
groupRoute.patch(
  "/share-location",
  validate(shareLocationSchema),
  verifyToken,
  ShareLocationWithGroup
);

module.exports = groupRoute;
