const express = require('express');
const { Register, Login } = require('../../controler/auth');
const { validate } = require('../../middleware/validate');
const { loginSchema } = require('../../validation/login');
const { registerSchema } = require('../../validation/register');

const authRouter = express.Router()

authRouter.post('/register',validate(registerSchema),Register)
authRouter.post('/login',validate(loginSchema),Login)

module.exports = authRouter 