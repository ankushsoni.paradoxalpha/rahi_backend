const express = require("express");
const {
  UpdateUser,
  DeleteUser,
  GetUser,
  UploadImage,
} = require("../../controler/user");
const { validate } = require("../../middleware/validate");
const {
  deleteUserSchema,
  uploadImageSchema,
} = require("../../validation/user");
const { verifyToken } = require("../../middleware/auth");
const { UploadFile } = require("../../helper/uploadFile");

const userRouter = express.Router();

userRouter.patch("/update", verifyToken, UpdateUser);
userRouter.delete(
  "/delete",
  validate(deleteUserSchema),

  verifyToken,
  DeleteUser
);
userRouter.get("/get-single-user", verifyToken, GetUser);
userRouter.put(
  "/upload-image",
  validate(uploadImageSchema),
  verifyToken,
  UploadFile.single("image"),
  UploadImage
);

module.exports = userRouter;
