const express = require("express");
const {
  SendNotification,
  GetNotifications,
} = require("../../controler/sendNotification");
const { verifyToken } = require("../../middleware/auth");

const notificationRoute = express.Router();

notificationRoute.post("/send", verifyToken, SendNotification);
notificationRoute.get("/get", verifyToken, GetNotifications);

module.exports = { notificationRoute };
