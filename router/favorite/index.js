const express = require("express")
const { AddFavoritePerson, GetAllFavoritPersonOfSingelUser, DeleteFavPerson } = require("../../controler/favorite")
const {validate} = require("../../middleware/validate")
const { AddFavoritePersonSchema, deleteFavPersonSchema } = require("../../validation/favorate")
const { verifyToken } = require("../../middleware/auth")
const favoritePersonRoute = express.Router()

favoritePersonRoute.post("/add-person",validate(AddFavoritePersonSchema),verifyToken ,AddFavoritePerson)
favoritePersonRoute.get("/get-all-person-single-user",verifyToken,GetAllFavoritPersonOfSingelUser)
favoritePersonRoute.delete("/remove-person",validate(deleteFavPersonSchema),verifyToken,DeleteFavPerson)

module.exports = favoritePersonRoute