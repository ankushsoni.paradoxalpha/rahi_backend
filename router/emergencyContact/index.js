const express = require("express")
const { AddEmergencyContact, GetAllEmergencyContactsOfSingelUser, RemovePersonFromEmergencyContactList } = require("../../controler/emergencyContact")
const {validate} = require("../../middleware/validate")
const { AddEmergencyContactSchema, RemovePersonFromEmergencyContactListSchema } = require("../../validation/emergencyContact")
const { verifyToken } = require("../../middleware/auth")

const emergencyContactRoute = express.Router()

emergencyContactRoute.post("/add-person",validate(AddEmergencyContactSchema),verifyToken,AddEmergencyContact)
emergencyContactRoute.get("/get-all-person",verifyToken,GetAllEmergencyContactsOfSingelUser)
emergencyContactRoute.delete("/remove-person",validate(RemovePersonFromEmergencyContactListSchema),verifyToken,RemovePersonFromEmergencyContactList)

module.exports = emergencyContactRoute
