const express = require("express");
const { verifyToken } = require("../../middleware/auth");
const { getMessage } = require("../../controler/chait");

const chaitRouter = express.Router();

chaitRouter.get("/get-message", verifyToken, getMessage);

module.exports = chaitRouter;
