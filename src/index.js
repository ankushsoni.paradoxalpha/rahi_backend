const express = require("express");
const helmet = require("helmet");
const cors = require("cors");
const userRouter = require("../router/user");
const dotenv = require("dotenv");
const groupRoute = require("../router/group");
const authRouter = require("../router/auth");
const favoritePersonRoute = require("../router/favorite");
const emergencyContactRoute = require("../router/emergencyContact");
const { notificationRoute } = require("../router/notification");
const chaitRouter = require("../router/chait");
const admin = require("firebase-admin");
const serviceAccount = require("../utils/serviceaccount_key.json");
const { Server } = require("socket.io");
const { Socket } = require("../socket");

dotenv.config({ path: ".env" });
const PORT = process.env.PORT || 4000;
const app = express();
app.use(express.json());
app.use(helmet());
app.use(
  cors({
    origin: [],
    methods: ["GET", "PUT", "POST", "DELETE"],
    allowedHeaders: ["Content-Type", "Authorization", "x-csrf-token"],
    credentials: true,
    maxAge: 600,
    exposedHeaders: ["*", "Authorization"],
  })
);

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  projectId: "rahi-fe821",
});

app.use("/auth", authRouter);
app.use("/user", userRouter);
app.use("/group", groupRoute);
app.use("/favorite", favoritePersonRoute);
app.use("/emergency-contact", emergencyContactRoute);
app.use("/notification", notificationRoute);
app.use("/chait", chaitRouter);

const runningServer = app.listen(PORT, () => {
  console.log(`Server is renning at ${PORT}`);
});

const io = new Server(runningServer, {
  pingTimeout: 60000,
  cors: {
    origin: "",
    methods: ["GET", "POST", "PUT", "DELETE", "PATCH"],
  },
});

Socket(io);
