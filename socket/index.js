const { sendMessages } = require("../controler/chait");

const Socket = (io) => {
  let users = [];
  const addUser = (userData, socketId) => {
    !users.some((user) => user.id === userData.id) &&
      users.push({ ...userData, socketId });
  };

  const getUser = (userId) => {
    return users.filter((user) => user?.id === userId);
  };

  const removeUser = (socketId) => {
    users = users.filter((user) => {
      user?.socketId !== socketId;
    });
  };

  // Socket.IO events
  io.on("connection", (socket) => {
    socket.on("addUser", (userData) => {
      console.log("A user connected");
      addUser(userData, socket.id);
    });
    socket.on("join_group", (user) => {
      socket.join(user.groupId);
      console.log("group joined");
    });
    socket.on("sendMessage", async (data) => {
      const user = getUser(data.user.id);
      await sendMessages(user, data);
      socket.to(user.socketId).emit("newMessage", data.message);
    });
    socket.on("disconnect", () => {
      console.log("A user disconnected");
      removeUser(socket.id);
    });
  });
};
module.exports = { Socket };
