const { PrismaClient } = require("@prisma/client");
const createError = require("../middleware/error");
const RequiredField = require("../components/fieldCheker");
const prisma = new PrismaClient();

module.exports = { prisma, createError, RequiredField };
