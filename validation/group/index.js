const yup = require("yup");
const createGroupSchema = yup.object({
  body: yup.object({
    name: yup.string().required(" Please send group name"),
  }),
});

const getAllMembersOfSingleGroupSchema = yup.object({
  query: yup.object({
    groupId: yup.string().required(" Please send groupId"),
  }),
});
const updateGroupSchema = yup.object({
  body: yup.object({
    groupId: yup.string().required(" Please send groupId"),
  }),
});
const deleteGroupSchema = yup.object({
  query: yup.object({
    groupId: yup.string().required(" Please send groupId"),
  }),
});
const getSingleGroupSchema = yup.object({
  query: yup.object({
    groupId: yup.string().required(" Please send groupId"),
  }),
});
const addMembersInGroupSchema = yup.object({
  body: yup.object({
    groupId: yup.string().required(" Please send your groupId"),
    members: yup.array().required(" Please send your members"),
  }),
});
const removeMemberFromGroupSchema = yup.object({
  query: yup.object({
    memberId: yup.string().required(" Please send your memberId"),
  }),
});
const joinGroupByGroupCode = yup.object({
  query: yup.object({
    groupCode: yup.string().required(" Please send your groupCode"),
  }),
});

const shareLocationSchema = yup.object({
  body: yup.object({
    groupMemberId: yup.string().required("Please provide groupMemberId"),
    shareLocation: yup
      .boolean()
      .required("Please provide shareLocation field. "),
  }),
});

module.exports = {
  joinGroupByGroupCode,
  getAllMembersOfSingleGroupSchema,
  updateGroupSchema,
  deleteGroupSchema,
  getSingleGroupSchema,
  addMembersInGroupSchema,
  removeMemberFromGroupSchema,
  createGroupSchema,
  shareLocationSchema,
};
