const yup  = require('yup');
const AddFavoritePersonSchema = yup.object({
    body:yup.object({
        favoritePersonEmail:yup.string().required("Please send favoritePersonEmail")
    })
})
const deleteFavPersonSchema = yup.object({
    query:yup.object({
        favoritePersonEmail:yup.string().required( " Please send favoritePersonEmail"),
    })
})

module.exports = {AddFavoritePersonSchema,deleteFavPersonSchema}
