const yup  = require('yup');
const registerSchema = yup.object({
    body:yup.object({
        name:yup.string().required( " Please enter your name"),
        email:yup.string().required( " Please enter your email"),
        password:yup.string().required("Please enter some password")
    })
})

module.exports = {registerSchema}
