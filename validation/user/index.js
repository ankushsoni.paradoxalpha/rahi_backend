const yup = require("yup");
const deleteUserSchema = yup.object({
  query: yup.object({
    userId: yup.string().required(" Please send your userId"),
  }),
});

const uploadImageSchema = yup.object({
  body: yup.object({
    userId: yup.string().required(" Please provide userId"),
  }),
  file: yup.object({
    image: yup.mixed().required("Please Provide Image"),
  }),
});

module.exports = { deleteUserSchema, uploadImageSchema };
