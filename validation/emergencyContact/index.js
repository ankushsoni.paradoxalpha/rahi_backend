const yup  = require('yup');
const AddEmergencyContactSchema = yup.object({
    body:yup.object({
        personEmail:yup.string().required("Please send personEmail")
    })
})

const RemovePersonFromEmergencyContactListSchema = yup.object({
    query:yup.object({
        personEmail:yup.string().required( " Please send your personEmail"),
    })
})

module.exports = {AddEmergencyContactSchema,RemovePersonFromEmergencyContactListSchema}
