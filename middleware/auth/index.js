const jwt = require("jsonwebtoken")

const verifyToken = (req, res, next) => {
    const token = req.header('Auth-token')
    if (!token) {
      res.status(401).json({ error: true, message: 'A token is required for authentication' })
    } else {
      try {
        if (process.env.SECRET_KEY) {
        
          let decoded = jwt.verify(token, process.env.SECRET_KEY);
          req.body.user = decoded;
          next();
      }
      } catch (error) {
        res.status(500).send({ error: true, message: error.message })
      }
    }
  }
  
module.exports =  {verifyToken}