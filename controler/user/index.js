const { prisma } = require("../../common");
const fs = require("fs");

const UploadImage = async (req, res) => {
  try {
    const file = req.file;
    const { userId } = req.body;
    const User = await prisma.user.findUnique({ where: { id: userId } });
    if (!User) {
      return res.status(401).json({ error: true, message: "Invalid userId" });
    }
    if (!file) {
      return res.status(400).json({ error: true, message: "No file uploaded" });
    }
    const imageUrl = req.file.location;

    await prisma.user.update({
      where: { id: userId },
      data: { profilePic: imageUrl },
    });

    return res.status(200).json({
      success: true,
      message: "Profile image uploaded successfully",
      imageUrl: imageUrl,
    });
  } catch (error) {
    return res
      .status(500)
      .json({ error: error, message: "Internal server error." });
  }
};

const UpdateUser = async (req, res) => {
  try {
    const { user, pointer, ...rest } = req.body;
    const userId = req.body.user.id;
    const User = await prisma.user.findUnique({ where: { id: userId } });
    if (!User) {
      return res.status(401).json({ error: true, message: "Invalid userId" });
    }
    await prisma.user.update({
      where: { id: userId },
      data: { ...rest, pointer },
    });
    return res
      .status(200)
      .json({ success: true, message: "User updated successfully" });
  } catch (error) {
    return res
      .status(500)
      .json({ error: error, message: "Internal server error." });
  }
};

const DeleteUser = async (req, res, next) => {
  try {
    const { userId } = req.query;
    const user = await prisma.user.findUnique({ where: { id: userId } });
    if (!user) {
      return res
        .status(404)
        .json({ error: true, message: "User not found, Invalid userId" });
    }
    await prisma.user.delete({
      where: { id: userId },
      include: { group: true, groupMember: true },
    });
    return res
      .status(200)
      .json({ success: true, message: "User deleted successfully" });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};

const GetUser = async (req, res, next) => {
  try {
    const id = req.body.user.id;
    const user = await prisma.user.findUnique({ where: { id } });
    if (!user) {
      return res.status(404).json({ error: true, message: "User not found" });
    }
    const { password, ...rest } = user;
    return res.status(200).json({ success: true, data: rest });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};

module.exports = { UpdateUser, DeleteUser, GetUser, UploadImage };
