const { prisma, RequiredField, createError } = require("../../common");
const { generateUniqueCode } = require("../../utils/randomNumber");

const CreateGroup = async (req, res, next) => {
  try {
    const { name, groupIcon } = req.body;
    const userId = req.body.user.id;
    const user = await prisma.user.findUnique({
      where: { id: userId },
      include: { group: true },
    });
    if (!user) {
      return res
        .status(401)
        .json({ error: true, message: "Invalid userId re-login or register." });
    }
    const isGroupExist = user.group.find((group) => group.name === name);
    if (isGroupExist) {
      return res.status(401).json({
        error: true,
        message:
          "You have already a group with this name, choose different name for creating new group",
      });
    }
    await prisma.group.create({
      data: {
        name,
        userId,
        groupIcon: groupIcon,
        groupCode: generateUniqueCode(),
        goupMembers: {
          create: [
            {
              email: user.email,
              admin: true,
              userId: user.id,
            },
          ],
        },
        admin: {
          create: {
            userId: user.id,
            admin: true,
          },
        },
      },
      include: { goupMembers: true, admin: true },
    });

    return res
      .status(200)
      .json({ success: true, message: "New group is created" });
  } catch (error) {
    return res
      .status(500)
      .json({ error: error, message: "Internal server error." });
  }
};

const GetAllGroupsOfSingleUser = async (req, res, next) => {
  try {
    const { pageNumber } = req.query;
    const userId = req.body.user.id;
    const userEmail = req.body.user.email;
    const user = await prisma.user.findUnique({ where: { id: userId } });
    if (!user) {
      return res.status(401).json({
        error: true,
        message: "Invailid  userId, re-login or register",
      });
    }
    const allgroups = await prisma.groupMember.findMany({
      where: { email: userEmail },
      select: { group: true },
    });
    let start = 10 * (parseInt(pageNumber ?? 1) - 1);
    let end = start > 0 ? start * 2 : 10;
    const groups = allgroups.slice(start, end);
    return res.status(200).json({ success: true, data: groups });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};

const GetAllMembersOfSingleGroup = async (req, res, next) => {
  try {
    const { groupId } = req.query;
    const groupMembers = await prisma.groupMember.findMany({
      where: { groupId },
      include: {
        user: {
          select: {
            name: true,
            email: true,
            nickName: true,
            profilePic: true,
            lastSean: true,
            location: true,
            batteryPercentage: true,
          },
        },
      },
    });

    return res.status(200).json({ success: true, data: groupMembers });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};

const GetAllMembersOfSingleGroup_2 = async (req, res, next) => {
  try {
    const { groupId } = req.query;
    const groupMembers = await prisma.groupMember.findMany({ where: { groupId }, select: { user: { select: { pointer:true,name:true } } } })
    
    return res
      .status(200)
      .json({ success: true, data: groupMembers });
  } catch (error) {
    return res.status(500).json({ error: true, message: "Internal server error." })

    return res.status(200).json({ success: true, data: groupMembers });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};

const UpdateGroup = async (req, res, next) => {
  try {
    const { groupId, name, description, groupIcon } = req.body;
    const isExist = await prisma.group.findUnique({ where: { id: groupId } });
    if (!isExist) {
      return res.status(404).json({ error: true, message: "Invalid groupId" });
    }
    await prisma.group.update({
      where: { id: groupId },
      data: {
        name,
        description,
        groupIcon,
      },
    });
    return res.status(200).json({ success: true, message: "Group Updated" });
  } catch (error) {
    return res
      .status(500)
      .json({ error: error, message: "Internal server error." });
  }
};

const DeleteGroup = async (req, res, next) => {
  try {
    const { groupId } = req.query;
    const group = await prisma.group.findUnique({ where: { id: groupId } });
    if (!group) {
      return res
        .status(401)
        .json({ error: true, message: "Invailid  groupID" });
    }
    await prisma.group.delete({
      where: { id: group.id },
      select: { goupMembers: true },
    });
    return res
      .status(200)
      .json({ success: true, message: "Group is deleted successfully" });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};
const GetSingleGroup = async (req, res, next) => {
  try {
    const { groupId } = req.query;
    const group = await prisma.group.findUnique({
      where: { id: groupId },
      include: { goupMembers: true, admin: true },
    });
    if (!group) {
      return res.status(401).json({
        error: true,
        message: "Invailid  userId, re-login or register",
      });
    }
    return res.status(200).json({ success: true, data: group });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};

const AddMembersInGroup = async (req, res, next) => {
  try {
    const { members, groupId } = req.body;
    const userId = req.body.user.id;
    if (!members.length > 0) {
      return res.status(500).json({
        error: true,
        message: "Group can't be empty, add atleast one member in it.",
      });
    }
    const adminData = await prisma.admin.findFirst({
      where: { AND: [{ groupId }, { userId }] },
    });
    if (!adminData || !adminData.admin) {
      return res.status(500).json({
        error: true,
        message: "You are not autherised to add members in group.",
      });
    }
    const groupMembers = await prisma.groupMember.findMany({
      where: { groupId },
    });
    let userNotExist = [];
    let userExist = [];
    let allredyIngroup = [];
    let i = 0;
    while (members.length > i) {
      let email = members[i].email;
      const isExist = await prisma.user.findUnique({ where: { email } });
      if (!isExist) {
        userNotExist.push(email);
        i++;
        continue;
      }
      const isAllradyExist = groupMembers.find(
        (m) => m.email === isExist.email
      );
      if (isExist && !isAllradyExist) {
        await prisma.groupMember.create({
          data: {
            groupId,
            email: isExist.email,
            userId: isExist.id,
          },
        });
        userExist.push(members[i].email);
      } else {
        isAllradyExist && allredyIngroup.push(isExist.email);
      }
      i++;
    }
    return res.status(200).json({
      success: true,
      message: `${userExist.length} new members are added in group.  ${
        userNotExist.length > 0 &&
        `[${userNotExist.join(", ")}] these emailId is not registerd `
      }. ${
        allredyIngroup.length > 0 &&
        `[${allredyIngroup.join(
          ", "
        )}] these Users are allready present in this group `
      }`,
    });
  } catch (error) {
    return res
      .status(500)
      .json({ error: error, message: "Internal server error." });
  }
};

const RemoveMemberFromGroup = async (req, res, next) => {
  try {
    const { memberId } = req.query;
    await prisma.groupMember.delete({ where: { id: memberId } });
    return res.status(200).json({
      success: true,
      message: `User removed successfully from this group.`,
    });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};

const GetAllGroups = async (req, res, next) => {
  try {
    const AllGroups = await prisma.group.findMany({});
    return res.status(200).json({ success: true, data: AllGroups });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};

const JoinGroupByGroupCode = async (req, res) => {
  try {
    const { groupCode } = req.query;
    const userId = req.body.user.id;
    const isExist = await prisma.group.findUnique({ where: { groupCode } });
    if (!isExist) {
      return res
        .status(404)
        .json({ error: true, message: "Invalid groupCode." });
    }
    const userDetails = await prisma.user.findUnique({ where: { id: userId } });
    if (!userDetails) {
      return res.status(404).json({ error: true, message: "User not found" });
    }
    const isUserAllreadyAdded = await prisma.groupMember.findFirst({
      where: { email: userDetails.email, groupId: isExist.id },
    });
    if (isUserAllreadyAdded) {
      return res
        .status(401)
        .json({ error: true, message: "You are allready added in this group" });
    }
    await prisma.groupMember.create({
      data: {
        groupId: isExist.id,
        email: userDetails.email,
        userId,
      },
    });
    return res
      .status(200)
      .json({ success: true, message: "You are added in group" });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};

const UpdateGroupMember = async (req, res) => {
  try {
    const { user, ...rest } = req.body;
    const userId = req.body.user.id;
    const currentUser = await prisma.groupMember.findFirst({
      where: { userId },
    });
    if (!user) {
      return res.status(404).json({ error: true, message: "User not found" });
    }
    await prisma.groupMember.update({
      where: { id: currentUser.id },
      data: { ...rest },
    });
    return res
      .status(200)
      .json({ success: true, message: "group member updated successfully" });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};

const ShareLocationWithGroup = async (req, res) => {
  try {
    const { groupMemberId, shareLocation } = req.body;
    const member = await prisma.groupMember.findUnique({
      where: { id: groupMemberId },
    });
    if (!member) {
      return res.status(404).json({ error: true, message: "Imvalid MemberId" });
    }
    await prisma.groupMember.update({
      where: { id: groupMemberId },
      data: { shareLocation },
    });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};

module.exports = {
  CreateGroup,
  GetAllGroups,
  GetAllGroupsOfSingleUser,
  GetAllMembersOfSingleGroup,
  GetAllMembersOfSingleGroup_2,
  UpdateGroup,
  DeleteGroup,
  GetSingleGroup,
  AddMembersInGroup,
  RemoveMemberFromGroup,
  JoinGroupByGroupCode,
  UpdateGroupMember,
  ShareLocationWithGroup,
};
