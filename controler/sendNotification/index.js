const { prisma } = require("../../common");
const { sendingNotification } = require("../../notification");

const SendNotification = async (req, res, next) => {
  try {
    const { title, body } = req.body;
    const userId = req.body.user.id;
    const { emergencyContact } = await prisma.user.findFirst({
      where: { id: userId },
      select: { emergencyContact: true },
    });
    for (let i = 0; emergencyContact.length > i; i++) {
      const { deviceToken, name } = await prisma.user.findUnique({
        where: { email: emergencyContact[i].email },
        select: { deviceToken: true, name: true },
      });
      if (deviceToken) {
        await prisma.notification.create({
          data: {
            userId: emergencyContact[i].personId,
            title,
            discription: body,
          },
        });
        await sendingNotification(title, body, deviceToken);
      }
    }
    return res.status(200).json({
      success: true,
      message: "Notification sent successfully",
    });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error" });
  }
};

const GetNotifications = async (req, res) => {
  try {
    const userId = req.body.user.id;
    const notifications = await prisma.user.findFirst({
      where: { id: userId },
      select: { id: true, notifications: true },
    });
    if (!notifications) {
      return res
        .status(404)
        .json({ error: true, message: "Invalid token login again." });
    }
    return res.status(200).json({ success: true, data: notifications });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error" });
  }
};

module.exports = { SendNotification, GetNotifications };
