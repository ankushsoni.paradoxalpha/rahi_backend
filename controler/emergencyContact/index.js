const { prisma } = require("../../common");

const AddEmergencyContact = async (req, res, next) => {
  try {
    const { personEmail } = req.body;
    const userId = req.body.user.id;
    const isPersonExist = await prisma.user.findUnique({
      where: { email: personEmail },
      select: { id: true, name: true, email: true },
    });
    if (!isPersonExist) {
      return res
        .status(404)
        .json({ error: true, message: "Person not exist, Wrong personEmail" });
    }
    const user = await prisma.user.findUnique({
      where: { id: userId },
      select: { id: true, emergencyContact: true },
    });
    if (!user) {
      return res.status(401).json({ error: true, message: "Invailid userId" });
    }
    const isExist = user.emergencyContact.find(
      (person) => person.personId === isPersonExist.id
    );
    if (isExist) {
      return res.status(200).json({
        error: true,
        message: "This person is already exist in your emergency contact list.",
      });
    }
    await prisma.emergencyContacts.create({
      data: {
        userId,
        personId: isPersonExist.id,
        name: isPersonExist.name,
        email: isPersonExist.email,
      },
    });
    return res.status(200).json({
      success: true,
      message: "Person is added in your emergency constact list.",
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};

const GetAllEmergencyContactsOfSingelUser = async (req, res, next) => {
  try {
    const { pageNumber } = req.query;
    const userId = req.body.user.id;
    let getOnlyItems = 10;
    let skipItems = getOnlyItems * (parseInt(pageNumber ?? 1) - 1);
    const emergencyContactList = await prisma.emergencyContacts.findMany({
      where: { userId },
      skip: skipItems,
      take: getOnlyItems,
    });
    if (!emergencyContactList) {
      return res.status(401).json({ error: true, message: "Invailid userId" });
    }
    return res.status(200).json({ success: true, data: emergencyContactList });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};

const RemovePersonFromEmergencyContactList = async (req, res, next) => {
  try {
    const { personEmail } = req.query;
    try {
      await prisma.emergencyContacts.delete({ where: { email: personEmail } });
      return res.status(200).json({
        success: true,
        message: `Person is removed from your emergency contact list.`,
      });
    } catch (error) {
      return res
        .status(404)
        .json({ error: true, message: "This email is not exist in databse." });
    }
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};

module.exports = {
  AddEmergencyContact,
  GetAllEmergencyContactsOfSingelUser,
  RemovePersonFromEmergencyContactList,
};
