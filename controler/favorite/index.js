const { prisma } = require("../../common");

const AddFavoritePerson = async (req, res, next) => {
  try {
    const { favoritePersonEmail } = req.body
    const userId = req.body.user.id
    const isPersonRegisterd = await prisma.user.findUnique({where:{email:favoritePersonEmail}})
    if(!isPersonRegisterd){
      return res.status(404).json({error:true,message:"Person not registeerd kindly invite him."})
    }
    const isPersonExist = await prisma.favorite.findFirst({
      where: {
        AND: [
          {
            userId,
          }, 
          {
            favoritePersonEmail
          }
        ]
      }
    })
    if (isPersonExist) {
      return res.status(404).json({ error: true, message: "User already exist in your favorite list" })
    }
    await prisma.favorite.create({ data: { userId, favoritePersonEmail } })
    return res.status(200).json({ success: true, message: "Person is added in your favorite list." })


  } catch (error) {
    return res.status(500).json({ error: true, message: "Internal server error" })
  }
}


const GetAllFavoritPersonOfSingelUser = async (req, res, next) => {
  try {
    const { pageNumber } = req.query
    const userId = req.body.user.id
    let getOnlyItems = 10;
    let skipItems = getOnlyItems * (parseInt(pageNumber ?? 1) - 1)
    let isUserExist = await prisma.user.findFirst({ where: { id: userId },select:{id:true,email:true,favorate:{select:{id:true,favoritePersonEmail:true}}} })
    
    if (!isUserExist) {
      return res.status(401).json({ error: true, message: "Invailid userId" })
    }
   for(let i=0;isUserExist.favorate.length>i;i++){
    const personData = await prisma.user.findUnique({where:{email:isUserExist.favorate[i].favoritePersonEmail},select:{email:true, name:true,nickName:true,profilePic:true,phoneNumber:true}})
    isUserExist.favorate[i] = {id:isUserExist.favorate[i].id,personData}
   }

    return res
      .status(200)
      .json({ success: true, data: isUserExist });
  } catch (error) {
    return res.status(500).json({ error: true, message: "Internal server error." })

  }
};

const DeleteFavPerson = async (req, res, next) => {
  try {
    const { favoritePersonEmail } = req.query
    const isExist = await prisma.favorite.findFirst({ where: { favoritePersonEmail: favoritePersonEmail } });
    if (!isExist) {
      return res.status(401).json({ error: true, message: "User not exist" })
    }
   await prisma.favorite.delete({ where: { id: isExist.id } });
    return res.status(200).json({ success: true, message: `Person is removed from your favorite person list.` })
  } catch (error) {
    return res.status(500).json({ error: true, message: "Internal server error." })
  }
}



module.exports = { AddFavoritePerson, GetAllFavoritPersonOfSingelUser, DeleteFavPerson }