const { prisma } = require("../../common");

const addChaitPerson = async (req, res) => {
  try {
    const userId = req.body.user.id;
    const { chaitPersonId } = req.query;
    const chaitPerson = await prisma.user.findUnique({
      where: { id: chaitPersonId },
      select: { id: true, name: true },
    });
    const user = await prisma.user.findUnique({
      where: { id: chaitPersonId },
      select: { id: true, name: true },
    });
    if (!chaitPerson || !user) {
      return res.status(404).json({
        error: true,
        message: !user ? "Invailid User login again" : "Invailid chaitPersonId",
      });
    }
    await prisma.ChaitRooms.create({
      data: {
        userId,
        userName: user.name,
        chaitPersonId,
        chaitPersonName: chaitPerson.name,
      },
    });
    return res
      .status(200)
      .json({ success: true, message: "chait room is created" });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};

const deleteChaitRoom = async (req, res) => {
  try {
    const { chaitRoomId } = req.query;
    const isExistChaitRoom = await prisma.ChaitRooms.findUnique({
      where: { id: chaitRoomId },
      select: { id: true },
    });
    if (!isExistChaitRoom) {
      return res.status(404).json({
        error: true,
        message: "Invailid chaitRoomId",
      });
    }
    await prisma.ChaitRooms.delete({ where: { id: chaitRoomId } });
    return res
      .status(200)
      .json({ success: true, message: "chait room is deleted" });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error." });
  }
};

module.exports = { addChaitPerson, deleteChaitRoom };
