const { prisma } = require("../../common");

const sendMessages = async (user, data) => {
  try {
    const userId = user.id;
    await prisma.message.create({
      data: {
        userId,
        text: data?.message,
        reciversId: data?.reciversId,
      },
    });
    return;
  } catch (error) {
    console.log({ error: "Failed to save message" });
  }
};

// Get all messages

const getMessage = async (req, res) => {
  try {
    const { chaitRoomId } = req.query;
    const messages = await prisma.message.findUnique({
      where: { chaitRoomId },
    });
    return res.status(200).json({ success: true, data: messages });
  } catch (error) {
    res
      .status(500)
      .json({ error: true, message: "Failed to retrieve messages" });
  }
};

module.exports = { sendMessages, getMessage };
