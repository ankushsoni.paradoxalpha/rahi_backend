const dotenv = require("dotenv");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { prisma } = require("../../common");

dotenv.config({ path: ".env" });

const Register = async (req, res) => {
  try {
    const { email, password, ...rest } = req.body;
    const user = await prisma.user.findUnique({ where: { email } });
    const hashedPassword = await bcrypt.hash(password, 10);
    if (user) {
      return res
        .status(401)
        .json({
          error: true,
          message: "This email is already exist with another account",
        });
    }
    await prisma.user.create({
      data: {
        email: email,
        password: hashedPassword,
        ...rest,
      },
    });
    return res.status(200).json({
      success: true,
      message: "successfuly registered",
    });
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error" });
  }
};

const Login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await prisma.user.findUnique({ where: { email } });
    if (!user) {
      return res
        .status(404)
        .json({
          error: true,
          message: "User not Found with this email, Register first",
        });
    }
    const isCorrectPassword = await bcrypt.compare(password, user.password);
    if (!isCorrectPassword) {
      return res
        .status(401)
        .json({ error: true, message: "Incorrect Password" });
    }
    {
      const token = jwt.sign(
        { id: user.id, email: user.email },
        process.env.SECRET_KEY
      );
      if (!token) {
        return res
          .status(500)
          .json({ error: true, message: "Token is not generated." });
      }

      return res.status(200).json({
        success: true,
        token,
      });
    }
  } catch (error) {
    return res
      .status(500)
      .json({ error: true, message: "Internal server error" });
  }
};

module.exports = { Register, Login };
