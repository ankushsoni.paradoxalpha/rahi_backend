const admin = require("firebase-admin");

const sendingNotification = async (title, body, deviceToken) => {
  const messaging = admin.messaging();

  const message = {
    notification: {
      title,
      body,
    },
    token: deviceToken,
  };
  messaging
    .send(message)
    .then((response) => {
      console.log("Successfully sent message:", response);
      return "Successfully sent message";
    })
    .catch((error) => {
      console.log("Error sending message:", error);
      return "Error sending message";
    });
};

module.exports = { sendingNotification };
