/*
  Warnings:

  - You are about to drop the column `groupId` on the `Message` table. All the data in the column will be lost.
  - You are about to drop the column `reciversId` on the `Message` table. All the data in the column will be lost.
  - You are about to drop the column `userId` on the `Message` table. All the data in the column will be lost.
  - Added the required column `originId` to the `Message` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Message" DROP COLUMN "groupId",
DROP COLUMN "reciversId",
DROP COLUMN "userId",
ADD COLUMN     "originId" TEXT NOT NULL;

-- CreateTable
CREATE TABLE "ChaitPersons" (
    "id" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "reciversId" TEXT NOT NULL,
    "userName" TEXT NOT NULL,
    "reciversName" TEXT NOT NULL,

    CONSTRAINT "ChaitPersons_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Message" ADD CONSTRAINT "Message_originId_fkey" FOREIGN KEY ("originId") REFERENCES "ChaitPersons"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
