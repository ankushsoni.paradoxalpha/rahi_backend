/*
  Warnings:

  - You are about to drop the `ChaitPersons` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "Message" DROP CONSTRAINT "Message_originId_fkey";

-- DropTable
DROP TABLE "ChaitPersons";

-- CreateTable
CREATE TABLE "ChaitRooms" (
    "id" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "chaitPersonId" TEXT NOT NULL,
    "userName" TEXT NOT NULL,
    "chaitPersonName" TEXT NOT NULL,

    CONSTRAINT "ChaitRooms_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Message" ADD CONSTRAINT "Message_originId_fkey" FOREIGN KEY ("originId") REFERENCES "ChaitRooms"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
