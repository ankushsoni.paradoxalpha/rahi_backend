/*
  Warnings:

  - You are about to drop the column `reciversId` on the `ChaitPersons` table. All the data in the column will be lost.
  - You are about to drop the column `reciversName` on the `ChaitPersons` table. All the data in the column will be lost.
  - Added the required column `chaitPersonId` to the `ChaitPersons` table without a default value. This is not possible if the table is not empty.
  - Added the required column `chaitPersonName` to the `ChaitPersons` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "ChaitPersons" DROP COLUMN "reciversId",
DROP COLUMN "reciversName",
ADD COLUMN     "chaitPersonId" TEXT NOT NULL,
ADD COLUMN     "chaitPersonName" TEXT NOT NULL;
