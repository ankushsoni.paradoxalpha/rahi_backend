/*
  Warnings:

  - A unique constraint covering the columns `[chaitRoomId]` on the table `Message` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "Message_chaitRoomId_key" ON "Message"("chaitRoomId");
