const createError = require("../../middleware/error");

const RequiredField = async (fields, src, next) => {
  const isFieldIdArray = typeof fields === "object";
  if (isFieldIdArray) {
    let value = Object.assign({}, src);
    for (let i = 0; fields.length > i; i++) {
      let isPresent = value[fields[i]];
      if (isPresent === undefined) {
        return next(createError(401, `${fields[i]} is required`)) ;
      }
    } 
    return true;
  } 
};

module.exports = RequiredField;
