 
function generateUniqueCode() {
  const maxCode = 999999; // Maximum value for 6 digit number
  const minCode = 100000; // Minimum value for 6 digit number
  const code = Math.floor(Math.random() * (maxCode - minCode + 1) + minCode); // Generate a random number between minCode and maxCode
  return code.toString(); // Return the 6 digit number as a string
}

module.exports = {generateUniqueCode}